 use storedpr_db
 
 DELIMiTER //
 CREATE TRIGGER insert_employee
	BEFORE INSERT
    ON employee for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT post 
		FROM post 
		WHERE post = NEW.post)      
        OR 
        NOT EXISTS (SELECT id 
        FROM pharmacy
        WHERE id = NEW.pharmacy_id)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'data with this id doesnt exist';
	  END IF;
 END//
 DELIMITER ;
 
 
  DELIMiTER //
 CREATE TRIGGER update_employee
	BEFORE UPDATE
    ON employee for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT post 
		FROM post 
		WHERE post = NEW.post) 
        OR 
        NOT EXISTS (SELECT id 
        FROM pharmacy
        WHERE id = NEW.pharmacy_id)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'data with this id doesnt exist';
	  END IF;
 END//
 DELIMITER ;
 
 
   DELIMiTER //
 CREATE TRIGGER delete_post
	BEFORE DELETE
    ON post for each row
 BEGIN
     IF (EXISTS(
		SELECT post 
		FROM employee 
		WHERE post = OLD.post)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not delete post';
	  END IF;
 END//
 DELIMITER ;
 
    DELIMiTER //
 CREATE TRIGGER update_post
	BEFORE UPDATE
    ON post for each row
 BEGIN
     IF (EXISTS(
		SELECT post
		FROM employee 
		WHERE post = OLD.post)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not update post';
	  END IF;
 END//
 DELIMITER ;
 
 
    DELIMiTER //
 CREATE TRIGGER delete_pharmacy
	BEFORE DELETE
    ON pharmacy for each row
 BEGIN
     IF (EXISTS(
		SELECT pharmacy_id 
		FROM employee 
		WHERE pharmacy_id = OLD.id)
        OR
        EXISTS(
        SELECT pharmacy_id
        FROM pharmacy_medicine
        WHERE pharmacy_id = OLD.id)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not delete pharmacy';
	  END IF;
 END//
 DELIMITER ;
 
 
    DELIMiTER //
 CREATE TRIGGER update_pharmacy
	BEFORE UPDATE
    ON pharmacy for each row
 BEGIN
     IF (EXISTS(
		SELECT pharmacy_id
		FROM employee 
		WHERE pharmacy_id = OLD.id)
        OR
        EXISTS(
        SELECT pharmacy_id
        FROM pharmacy_medicine
        WHERE pharmacy_id = OLD.id))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not update pharmacy';
	  END IF;
	IF (NOT EXISTS(
		SELECT street 
		FROM street 
		WHERE street = NEW.street)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Data with this id doesnt exist';
	  END IF;
 END//
 DELIMITER ;
 
 
     DELIMiTER //
 CREATE TRIGGER insert_pharmacy
	BEFORE INSERT
    ON pharmacy for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT street 
		FROM street 
		WHERE street = NEW.street)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Data with this id doesnt exist';
	  END IF;
 END//
 
 
    DELIMiTER //
 CREATE TRIGGER delete_street
	BEFORE DELETE
    ON street for each row
 BEGIN
     IF (EXISTS(
		SELECT street 
		FROM pharmacy 
		WHERE street = OLD.street)) 
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not delete street';
	  END IF;
 END//
 
 
 DELIMITER ;  
 
 
 DELIMiTER //
 CREATE TRIGGER update_street
	BEFORE UPDATE
    ON street for each row
 BEGIN
     IF (EXISTS(
		SELECT street 
		FROM pharmacy 
		WHERE street = OLD.street))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not delete street';
	  END IF;
 END//
 DELIMITER ;
 
  DELIMiTER //
 CREATE TRIGGER insert_pharmacy_medicine
	BEFORE INSERT
    ON pharmacy_medicine for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT id 
		FROM pharmacy 
		WHERE id = NEW.pharmacy_id)
        OR
        NOT EXISTS(
        SELECT id 
        FROM medicine
        WHERE id = NEW.medicine_id))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not insert data';
	  END IF;
 END//
 DELIMITER ;
 
   DELIMiTER //
 CREATE TRIGGER update_pharmacy_medicine
	BEFORE UPDATE
    ON pharmacy_medicine for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT id 
		FROM pharmacy 
		WHERE id = NEW.pharmacy_id)
        OR
        NOT EXISTS(
        SELECT id 
        FROM medicine
        WHERE id = NEW.medicine_id))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not update data';
	  END IF;
 END//
 DELIMITER ;
 
 
   DELIMiTER //
 CREATE TRIGGER insert_medicine_zone
	BEFORE INSERT
    ON medicine_zone for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT id 
		FROM medicine 
		WHERE id = NEW.medicine_id)
        OR
        NOT EXISTS(
        SELECT id 
        FROM zone
        WHERE id = NEW.zone_id))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not insert data';
	  END IF;
 END//
 DELIMITER ;
 
    DELIMiTER //
 CREATE TRIGGER update_medicine_zone
	BEFORE UPDATE
    ON medicine_zone for each row
 BEGIN
     IF (NOT EXISTS(
		SELECT id 
		FROM medicine 
		WHERE id = NEW.medicine_id)
        OR
        NOT EXISTS(
        SELECT id 
        FROM zone
        WHERE id = NEW.zone_id))
		THEN SIGNAL SQLSTATE '45000'
			SET MESSAGE_TEXT = 'Can not update data';
	  END IF;
 END//
 DELIMITER ;
 
 
 /* SECOND TASK */
 
 DELIMITER //
 CREATE TRIGGER check_identity_number
	BEFORE INSERT 
    ON employee for each row
BEGIN
	IF(NEW.identity_number RLIKE '0{2}$')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'identity number can not have two zero in the end ' ;
	END IF;
END //
DELIMITER ;


	/* THIRD TASK */


 DELIMITER //
 CREATE TRIGGER ministry_code_check
	BEFORE INSERT 
    ON medicine for each row
BEGIN
	IF(NEW.ministry_code NOT RLIKE '^[А-Я]{2}-[0-9]{3}-[0-9]{2}')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'can not insert this data' ;
	END IF;
END //
DELIMITER ;


	/* fourth TASK */


 DELIMITER //
 CREATE TRIGGER ban_update
	BEFORE UPDATE 
    ON post
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'can not update this table' ;
DELIMITER ;
 

 
 
 
 
 
 